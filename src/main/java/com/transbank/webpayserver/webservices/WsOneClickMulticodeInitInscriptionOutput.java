
package com.transbank.webpayserver.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para wsOneClickMulticodeInitInscriptionOutput complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="wsOneClickMulticodeInitInscriptionOutput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://service.wswebpay.webpay.transbank.com/}baseBean"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="urlInscriptionForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wsOneClickMulticodeInitInscriptionOutput", propOrder = {
    "token",
    "urlInscriptionForm"
})
public class WsOneClickMulticodeInitInscriptionOutput
    extends BaseBean
{

    protected String token;
    protected String urlInscriptionForm;

    /**
     * Obtiene el valor de la propiedad token.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Define el valor de la propiedad token.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Obtiene el valor de la propiedad urlInscriptionForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlInscriptionForm() {
        return urlInscriptionForm;
    }

    /**
     * Define el valor de la propiedad urlInscriptionForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlInscriptionForm(String value) {
        this.urlInscriptionForm = value;
    }

}
