
package com.transbank.webpayserver.webservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.transbank.webpayserver.webservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Authorize_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "authorize");
    private final static QName _AuthorizeResponse_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "authorizeResponse");
    private final static QName _FinishInscription_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "finishInscription");
    private final static QName _FinishInscriptionResponse_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "finishInscriptionResponse");
    private final static QName _InitInscription_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "initInscription");
    private final static QName _InitInscriptionResponse_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "initInscriptionResponse");
    private final static QName _Nullify_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "nullify");
    private final static QName _NullifyResponse_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "nullifyResponse");
    private final static QName _RemoveInscription_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "removeInscription");
    private final static QName _RemoveInscriptionResponse_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "removeInscriptionResponse");
    private final static QName _Reverse_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "reverse");
    private final static QName _ReverseNullification_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "reverseNullification");
    private final static QName _ReverseNullificationResponse_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "reverseNullificationResponse");
    private final static QName _ReverseResponse_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "reverseResponse");
    private final static QName _WsOneClickMulticodeFinishInscriptionInput_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "wsOneClickMulticodeFinishInscriptionInput");
    private final static QName _WsOneClickMulticodeFinishInscriptionOutput_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "wsOneClickMulticodeFinishInscriptionOutput");
    private final static QName _WsOneClickMulticodeInitInscriptionInput_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "wsOneClickMulticodeInitInscriptionInput");
    private final static QName _WsOneClickMulticodeInitInscriptionOutput_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "wsOneClickMulticodeInitInscriptionOutput");
    private final static QName _WsOneClickMulticodeRemoveInscriptionInput_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "wsOneClickMulticodeRemoveInscriptionInput");
    private final static QName _WsOneClickMulticodeRemoveInscriptionOutput_QNAME = new QName("http://service.wswebpay.webpay.transbank.com/", "wsOneClickMulticodeRemoveInscriptionOutput");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.transbank.webpayserver.webservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Authorize }
     * 
     */
    public Authorize createAuthorize() {
        return new Authorize();
    }

    /**
     * Create an instance of {@link AuthorizeResponse }
     * 
     */
    public AuthorizeResponse createAuthorizeResponse() {
        return new AuthorizeResponse();
    }

    /**
     * Create an instance of {@link FinishInscription }
     * 
     */
    public FinishInscription createFinishInscription() {
        return new FinishInscription();
    }

    /**
     * Create an instance of {@link FinishInscriptionResponse }
     * 
     */
    public FinishInscriptionResponse createFinishInscriptionResponse() {
        return new FinishInscriptionResponse();
    }

    /**
     * Create an instance of {@link InitInscription }
     * 
     */
    public InitInscription createInitInscription() {
        return new InitInscription();
    }

    /**
     * Create an instance of {@link InitInscriptionResponse }
     * 
     */
    public InitInscriptionResponse createInitInscriptionResponse() {
        return new InitInscriptionResponse();
    }

    /**
     * Create an instance of {@link Nullify }
     * 
     */
    public Nullify createNullify() {
        return new Nullify();
    }

    /**
     * Create an instance of {@link NullifyResponse }
     * 
     */
    public NullifyResponse createNullifyResponse() {
        return new NullifyResponse();
    }

    /**
     * Create an instance of {@link RemoveInscription }
     * 
     */
    public RemoveInscription createRemoveInscription() {
        return new RemoveInscription();
    }

    /**
     * Create an instance of {@link RemoveInscriptionResponse }
     * 
     */
    public RemoveInscriptionResponse createRemoveInscriptionResponse() {
        return new RemoveInscriptionResponse();
    }

    /**
     * Create an instance of {@link Reverse }
     * 
     */
    public Reverse createReverse() {
        return new Reverse();
    }

    /**
     * Create an instance of {@link ReverseNullification }
     * 
     */
    public ReverseNullification createReverseNullification() {
        return new ReverseNullification();
    }

    /**
     * Create an instance of {@link ReverseNullificationResponse }
     * 
     */
    public ReverseNullificationResponse createReverseNullificationResponse() {
        return new ReverseNullificationResponse();
    }

    /**
     * Create an instance of {@link ReverseResponse }
     * 
     */
    public ReverseResponse createReverseResponse() {
        return new ReverseResponse();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeFinishInscriptionInput }
     * 
     */
    public WsOneClickMulticodeFinishInscriptionInput createWsOneClickMulticodeFinishInscriptionInput() {
        return new WsOneClickMulticodeFinishInscriptionInput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeFinishInscriptionOutput }
     * 
     */
    public WsOneClickMulticodeFinishInscriptionOutput createWsOneClickMulticodeFinishInscriptionOutput() {
        return new WsOneClickMulticodeFinishInscriptionOutput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeInitInscriptionInput }
     * 
     */
    public WsOneClickMulticodeInitInscriptionInput createWsOneClickMulticodeInitInscriptionInput() {
        return new WsOneClickMulticodeInitInscriptionInput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeInitInscriptionOutput }
     * 
     */
    public WsOneClickMulticodeInitInscriptionOutput createWsOneClickMulticodeInitInscriptionOutput() {
        return new WsOneClickMulticodeInitInscriptionOutput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeRemoveInscriptionInput }
     * 
     */
    public WsOneClickMulticodeRemoveInscriptionInput createWsOneClickMulticodeRemoveInscriptionInput() {
        return new WsOneClickMulticodeRemoveInscriptionInput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeRemoveInscriptionOutput }
     * 
     */
    public WsOneClickMulticodeRemoveInscriptionOutput createWsOneClickMulticodeRemoveInscriptionOutput() {
        return new WsOneClickMulticodeRemoveInscriptionOutput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeNullificationInput }
     * 
     */
    public WsOneClickMulticodeNullificationInput createWsOneClickMulticodeNullificationInput() {
        return new WsOneClickMulticodeNullificationInput();
    }

    /**
     * Create an instance of {@link BaseBean }
     * 
     */
    public BaseBean createBaseBean() {
        return new BaseBean();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeNullificationOutput }
     * 
     */
    public WsOneClickMulticodeNullificationOutput createWsOneClickMulticodeNullificationOutput() {
        return new WsOneClickMulticodeNullificationOutput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodePaymentInput }
     * 
     */
    public WsOneClickMulticodePaymentInput createWsOneClickMulticodePaymentInput() {
        return new WsOneClickMulticodePaymentInput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeStorePaymentInput }
     * 
     */
    public WsOneClickMulticodeStorePaymentInput createWsOneClickMulticodeStorePaymentInput() {
        return new WsOneClickMulticodeStorePaymentInput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodePaymentOutput }
     * 
     */
    public WsOneClickMulticodePaymentOutput createWsOneClickMulticodePaymentOutput() {
        return new WsOneClickMulticodePaymentOutput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeStorePaymentOutput }
     * 
     */
    public WsOneClickMulticodeStorePaymentOutput createWsOneClickMulticodeStorePaymentOutput() {
        return new WsOneClickMulticodeStorePaymentOutput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeReverseNullificationInput }
     * 
     */
    public WsOneClickMulticodeReverseNullificationInput createWsOneClickMulticodeReverseNullificationInput() {
        return new WsOneClickMulticodeReverseNullificationInput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeReverseNullificationOutput }
     * 
     */
    public WsOneClickMulticodeReverseNullificationOutput createWsOneClickMulticodeReverseNullificationOutput() {
        return new WsOneClickMulticodeReverseNullificationOutput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeReverseInput }
     * 
     */
    public WsOneClickMulticodeReverseInput createWsOneClickMulticodeReverseInput() {
        return new WsOneClickMulticodeReverseInput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeReverseOutput }
     * 
     */
    public WsOneClickMulticodeReverseOutput createWsOneClickMulticodeReverseOutput() {
        return new WsOneClickMulticodeReverseOutput();
    }

    /**
     * Create an instance of {@link WsOneClickMulticodeStoreReverseOutput }
     * 
     */
    public WsOneClickMulticodeStoreReverseOutput createWsOneClickMulticodeStoreReverseOutput() {
        return new WsOneClickMulticodeStoreReverseOutput();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Authorize }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "authorize")
    public JAXBElement<Authorize> createAuthorize(Authorize value) {
        return new JAXBElement<Authorize>(_Authorize_QNAME, Authorize.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "authorizeResponse")
    public JAXBElement<AuthorizeResponse> createAuthorizeResponse(AuthorizeResponse value) {
        return new JAXBElement<AuthorizeResponse>(_AuthorizeResponse_QNAME, AuthorizeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishInscription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "finishInscription")
    public JAXBElement<FinishInscription> createFinishInscription(FinishInscription value) {
        return new JAXBElement<FinishInscription>(_FinishInscription_QNAME, FinishInscription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishInscriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "finishInscriptionResponse")
    public JAXBElement<FinishInscriptionResponse> createFinishInscriptionResponse(FinishInscriptionResponse value) {
        return new JAXBElement<FinishInscriptionResponse>(_FinishInscriptionResponse_QNAME, FinishInscriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitInscription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "initInscription")
    public JAXBElement<InitInscription> createInitInscription(InitInscription value) {
        return new JAXBElement<InitInscription>(_InitInscription_QNAME, InitInscription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitInscriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "initInscriptionResponse")
    public JAXBElement<InitInscriptionResponse> createInitInscriptionResponse(InitInscriptionResponse value) {
        return new JAXBElement<InitInscriptionResponse>(_InitInscriptionResponse_QNAME, InitInscriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Nullify }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "nullify")
    public JAXBElement<Nullify> createNullify(Nullify value) {
        return new JAXBElement<Nullify>(_Nullify_QNAME, Nullify.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NullifyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "nullifyResponse")
    public JAXBElement<NullifyResponse> createNullifyResponse(NullifyResponse value) {
        return new JAXBElement<NullifyResponse>(_NullifyResponse_QNAME, NullifyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveInscription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "removeInscription")
    public JAXBElement<RemoveInscription> createRemoveInscription(RemoveInscription value) {
        return new JAXBElement<RemoveInscription>(_RemoveInscription_QNAME, RemoveInscription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveInscriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "removeInscriptionResponse")
    public JAXBElement<RemoveInscriptionResponse> createRemoveInscriptionResponse(RemoveInscriptionResponse value) {
        return new JAXBElement<RemoveInscriptionResponse>(_RemoveInscriptionResponse_QNAME, RemoveInscriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Reverse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "reverse")
    public JAXBElement<Reverse> createReverse(Reverse value) {
        return new JAXBElement<Reverse>(_Reverse_QNAME, Reverse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReverseNullification }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "reverseNullification")
    public JAXBElement<ReverseNullification> createReverseNullification(ReverseNullification value) {
        return new JAXBElement<ReverseNullification>(_ReverseNullification_QNAME, ReverseNullification.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReverseNullificationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "reverseNullificationResponse")
    public JAXBElement<ReverseNullificationResponse> createReverseNullificationResponse(ReverseNullificationResponse value) {
        return new JAXBElement<ReverseNullificationResponse>(_ReverseNullificationResponse_QNAME, ReverseNullificationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReverseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "reverseResponse")
    public JAXBElement<ReverseResponse> createReverseResponse(ReverseResponse value) {
        return new JAXBElement<ReverseResponse>(_ReverseResponse_QNAME, ReverseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WsOneClickMulticodeFinishInscriptionInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "wsOneClickMulticodeFinishInscriptionInput")
    public JAXBElement<WsOneClickMulticodeFinishInscriptionInput> createWsOneClickMulticodeFinishInscriptionInput(WsOneClickMulticodeFinishInscriptionInput value) {
        return new JAXBElement<WsOneClickMulticodeFinishInscriptionInput>(_WsOneClickMulticodeFinishInscriptionInput_QNAME, WsOneClickMulticodeFinishInscriptionInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WsOneClickMulticodeFinishInscriptionOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "wsOneClickMulticodeFinishInscriptionOutput")
    public JAXBElement<WsOneClickMulticodeFinishInscriptionOutput> createWsOneClickMulticodeFinishInscriptionOutput(WsOneClickMulticodeFinishInscriptionOutput value) {
        return new JAXBElement<WsOneClickMulticodeFinishInscriptionOutput>(_WsOneClickMulticodeFinishInscriptionOutput_QNAME, WsOneClickMulticodeFinishInscriptionOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WsOneClickMulticodeInitInscriptionInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "wsOneClickMulticodeInitInscriptionInput")
    public JAXBElement<WsOneClickMulticodeInitInscriptionInput> createWsOneClickMulticodeInitInscriptionInput(WsOneClickMulticodeInitInscriptionInput value) {
        return new JAXBElement<WsOneClickMulticodeInitInscriptionInput>(_WsOneClickMulticodeInitInscriptionInput_QNAME, WsOneClickMulticodeInitInscriptionInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WsOneClickMulticodeInitInscriptionOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "wsOneClickMulticodeInitInscriptionOutput")
    public JAXBElement<WsOneClickMulticodeInitInscriptionOutput> createWsOneClickMulticodeInitInscriptionOutput(WsOneClickMulticodeInitInscriptionOutput value) {
        return new JAXBElement<WsOneClickMulticodeInitInscriptionOutput>(_WsOneClickMulticodeInitInscriptionOutput_QNAME, WsOneClickMulticodeInitInscriptionOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WsOneClickMulticodeRemoveInscriptionInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "wsOneClickMulticodeRemoveInscriptionInput")
    public JAXBElement<WsOneClickMulticodeRemoveInscriptionInput> createWsOneClickMulticodeRemoveInscriptionInput(WsOneClickMulticodeRemoveInscriptionInput value) {
        return new JAXBElement<WsOneClickMulticodeRemoveInscriptionInput>(_WsOneClickMulticodeRemoveInscriptionInput_QNAME, WsOneClickMulticodeRemoveInscriptionInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WsOneClickMulticodeRemoveInscriptionOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wswebpay.webpay.transbank.com/", name = "wsOneClickMulticodeRemoveInscriptionOutput")
    public JAXBElement<WsOneClickMulticodeRemoveInscriptionOutput> createWsOneClickMulticodeRemoveInscriptionOutput(WsOneClickMulticodeRemoveInscriptionOutput value) {
        return new JAXBElement<WsOneClickMulticodeRemoveInscriptionOutput>(_WsOneClickMulticodeRemoveInscriptionOutput_QNAME, WsOneClickMulticodeRemoveInscriptionOutput.class, null, value);
    }

}
