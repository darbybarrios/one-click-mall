
package com.transbank.webpayserver.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para baseBean complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="baseBean"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "baseBean")
@XmlSeeAlso({
    WsOneClickMulticodeFinishInscriptionInput.class,
    WsOneClickMulticodeFinishInscriptionOutput.class,
    WsOneClickMulticodeInitInscriptionInput.class,
    WsOneClickMulticodeInitInscriptionOutput.class,
    WsOneClickMulticodeRemoveInscriptionInput.class,
    WsOneClickMulticodeRemoveInscriptionOutput.class,
    WsOneClickMulticodeNullificationInput.class,
    WsOneClickMulticodeNullificationOutput.class,
    WsOneClickMulticodePaymentInput.class,
    WsOneClickMulticodeStorePaymentInput.class,
    WsOneClickMulticodePaymentOutput.class,
    WsOneClickMulticodeStorePaymentOutput.class,
    WsOneClickMulticodeReverseNullificationInput.class,
    WsOneClickMulticodeReverseNullificationOutput.class,
    WsOneClickMulticodeReverseInput.class,
    WsOneClickMulticodeReverseOutput.class,
    WsOneClickMulticodeStoreReverseOutput.class
})
public class BaseBean {


}
