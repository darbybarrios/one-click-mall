
package com.transbank.webpayserver.webservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para wsOneClickMulticodeReverseOutput complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="wsOneClickMulticodeReverseOutput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://service.wswebpay.webpay.transbank.com/}baseBean"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="storesReverse" type="{http://service.wswebpay.webpay.transbank.com/}wsOneClickMulticodeStoreReverseOutput" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wsOneClickMulticodeReverseOutput", propOrder = {
    "storesReverse"
})
public class WsOneClickMulticodeReverseOutput
    extends BaseBean
{

    protected List<WsOneClickMulticodeStoreReverseOutput> storesReverse;

    /**
     * Gets the value of the storesReverse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the storesReverse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStoresReverse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsOneClickMulticodeStoreReverseOutput }
     * 
     * 
     */
    public List<WsOneClickMulticodeStoreReverseOutput> getStoresReverse() {
        if (storesReverse == null) {
            storesReverse = new ArrayList<WsOneClickMulticodeStoreReverseOutput>();
        }
        return this.storesReverse;
    }

}
