
package com.transbank.webpayserver.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para wsOneClickMulticodeRemoveInscriptionInput complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="wsOneClickMulticodeRemoveInscriptionInput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://service.wswebpay.webpay.transbank.com/}baseBean"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tbkUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wsOneClickMulticodeRemoveInscriptionInput", propOrder = {
    "tbkUser",
    "username"
})
public class WsOneClickMulticodeRemoveInscriptionInput
    extends BaseBean
{

    protected String tbkUser;
    protected String username;

    /**
     * Obtiene el valor de la propiedad tbkUser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTbkUser() {
        return tbkUser;
    }

    /**
     * Define el valor de la propiedad tbkUser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTbkUser(String value) {
        this.tbkUser = value;
    }

    /**
     * Obtiene el valor de la propiedad username.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Define el valor de la propiedad username.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

}
