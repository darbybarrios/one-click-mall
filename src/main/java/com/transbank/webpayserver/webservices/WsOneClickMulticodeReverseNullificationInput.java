
package com.transbank.webpayserver.webservices;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para wsOneClickMulticodeReverseNullificationInput complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="wsOneClickMulticodeReverseNullificationInput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://service.wswebpay.webpay.transbank.com/}baseBean"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="commerceId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="buyOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nullifyAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wsOneClickMulticodeReverseNullificationInput", propOrder = {
    "commerceId",
    "buyOrder",
    "nullifyAmount"
})
public class WsOneClickMulticodeReverseNullificationInput
    extends BaseBean
{

    protected Long commerceId;
    protected String buyOrder;
    protected BigDecimal nullifyAmount;

    /**
     * Obtiene el valor de la propiedad commerceId.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCommerceId() {
        return commerceId;
    }

    /**
     * Define el valor de la propiedad commerceId.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCommerceId(Long value) {
        this.commerceId = value;
    }

    /**
     * Obtiene el valor de la propiedad buyOrder.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyOrder() {
        return buyOrder;
    }

    /**
     * Define el valor de la propiedad buyOrder.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyOrder(String value) {
        this.buyOrder = value;
    }

    /**
     * Obtiene el valor de la propiedad nullifyAmount.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNullifyAmount() {
        return nullifyAmount;
    }

    /**
     * Define el valor de la propiedad nullifyAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNullifyAmount(BigDecimal value) {
        this.nullifyAmount = value;
    }

}
