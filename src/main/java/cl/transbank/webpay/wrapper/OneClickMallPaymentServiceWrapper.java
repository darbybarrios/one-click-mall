package cl.transbank.webpay.wrapper;

import cl.transbank.webpay.Webpay;
import cl.transbank.webpay.security.SoapSignature;
import com.transbank.webpayserver.webservices.Authorize;
import com.transbank.webpayserver.webservices.FinishInscription;
import com.transbank.webpayserver.webservices.InitInscription;
import com.transbank.webpayserver.webservices.Nullify;
import com.transbank.webpayserver.webservices.RemoveInscription;
import com.transbank.webpayserver.webservices.Reverse;
import com.transbank.webpayserver.webservices.ReverseNullification;
import com.transbank.webpayserver.webservices.WSOneClickMulticodeService;
import com.transbank.webpayserver.webservices.WSOneClickMulticodeServiceImplService;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeFinishInscriptionOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeInitInscriptionOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeNullificationOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodePaymentOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeRemoveInscriptionOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeReverseNullificationOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeReverseOutput;


public class OneClickMallPaymentServiceWrapper extends ServiceWrapperBase {

  private WSOneClickMulticodeService port;

  protected OneClickMallPaymentServiceWrapper(Webpay.Environment environment, SoapSignature signature) throws Exception {
    super(environment, signature);
    this.port = initPort(
        WSOneClickMulticodeService.class,
        WSOneClickMulticodeServiceImplService.SERVICE,
        WSOneClickMulticodeServiceImplService.WSOneClickMulticodeServiceImplPort,
        "oneclickmall.wsdl"
    );
  }

  public WsOneClickMulticodeInitInscriptionOutput initInscription(InitInscription input) {
    return port.initInscription(input).getReturn();
  }

  public WsOneClickMulticodeFinishInscriptionOutput finishInscription(FinishInscription input) {
    return port.finishInscription(input).getReturn();
  }

  public WsOneClickMulticodePaymentOutput authorize(Authorize input) {
    return port.authorize(input).getReturn();
  }

  public WsOneClickMulticodeReverseOutput reverseTransaction(Reverse input) {
    return port.reverse(input).getReturn();
  }

  public WsOneClickMulticodeRemoveInscriptionOutput removeInscription(RemoveInscription input) {
    return port.removeInscription(input).getReturn();
  }

  public WsOneClickMulticodeNullificationOutput nullify(Nullify input) {
    return port.nullify(input).getReturn();
  }

  public WsOneClickMulticodeReverseNullificationOutput reverseNullification(ReverseNullification input) {
    return port.reverseNullification(input).getReturn();
  }

}
