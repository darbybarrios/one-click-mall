package cl.transbank.webpay;

import cl.transbank.webpay.security.SoapSignature;
import cl.transbank.webpay.wrapper.OneClickMallPaymentServiceWrapper;
import com.transbank.webpayserver.webservices.Authorize;
import com.transbank.webpayserver.webservices.FinishInscription;
import com.transbank.webpayserver.webservices.InitInscription;
import com.transbank.webpayserver.webservices.Nullify;
import com.transbank.webpayserver.webservices.RemoveInscription;
import com.transbank.webpayserver.webservices.Reverse;
import com.transbank.webpayserver.webservices.ReverseNullification;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeFinishInscriptionInput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeFinishInscriptionOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeInitInscriptionInput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeInitInscriptionOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeNullificationOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodePaymentInput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodePaymentOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeRemoveInscriptionOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeReverseNullificationOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeReverseOutput;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeStorePaymentInput;

import java.math.BigDecimal;

public class WebpayOneClickMall extends OneClickMallPaymentServiceWrapper {
  String commerceCode;

  public WebpayOneClickMall(Webpay.Environment mode, String commerceCode, SoapSignature signature) throws Exception {
    super(mode, signature);
    this.commerceCode = commerceCode;
  }

  public WsOneClickMulticodeInitInscriptionOutput initInscription(String username, String email, String urlReturn) {
    InitInscription initInscription = new InitInscription();
    WsOneClickMulticodeInitInscriptionInput input = new WsOneClickMulticodeInitInscriptionInput();
    input.setUsername(username);
    input.setEmail(email);
    input.setReturnUrl(urlReturn);
    initInscription.setInput(input);
    return this.initInscription(initInscription);
  }

  public WsOneClickMulticodeFinishInscriptionOutput finishInscription(String token) {
    FinishInscription finishInscription = new FinishInscription();
    WsOneClickMulticodeFinishInscriptionInput input = new WsOneClickMulticodeFinishInscriptionInput();
    input.setToken(token);
    finishInscription.setInput(input);
    return this.finishInscription(finishInscription);
  }

  public WsOneClickMulticodePaymentOutput authorize(String buyOrder, String tbkUser, String username, BigDecimal amount, Long commerceId) {
    Authorize authorize = new Authorize();
    WsOneClickMulticodeStorePaymentInput storePaymentInput = new WsOneClickMulticodeStorePaymentInput();
    storePaymentInput.setAmount(amount);
    storePaymentInput.setBuyOrder(buyOrder);
    storePaymentInput.setCommerceId(commerceId);
    WsOneClickMulticodePaymentInput input = new WsOneClickMulticodePaymentInput();
    input.setBuyOrder(buyOrder);
    input.setTbkUser(tbkUser);
    input.setUsername(username);
    input.getStoresInput().add(storePaymentInput);
    authorize.setInput(input);
    return this.authorize(authorize);
  }

  public WsOneClickMulticodeReverseOutput reverseTransaction(Reverse input) {
    return this.reverseTransaction(input);
  }

  public WsOneClickMulticodeRemoveInscriptionOutput removeInscription(RemoveInscription input) {
    return this.removeInscription(input);
  }

  public WsOneClickMulticodeNullificationOutput nullify(Nullify input) {
    return this.nullify(input);
  }

  public WsOneClickMulticodeReverseNullificationOutput ReverseNullification(ReverseNullification input) {
    return this.reverseNullification(input);
  }
}
