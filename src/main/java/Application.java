import cl.transbank.webpay.Webpay;
import cl.transbank.webpay.WebpayOneClickMall;
import cl.transbank.webpay.configuration.Configuration;
import com.transbank.webpayserver.webservices.WsOneClickMulticodeInitInscriptionOutput;

public class Application {
  public static void main(String[] args) throws Exception {

    WebpayOneClickMall webpayOneClickMall = new Webpay(Configuration.forTestingOneClickMall()).getOneClickMallTransaction();
    StringBuilder stringBuilder = new StringBuilder();
    String username = "pepito";
    String email = "pepito@gmail.com";
    String urlReturn = "http://localhost:8080/resultado/de/transaccion";
    WsOneClickMulticodeInitInscriptionOutput result = webpayOneClickMall.initInscription(username, email, urlReturn);
    stringBuilder.append(result.getUrlInscriptionForm());
    stringBuilder.append("?TBK_TOKEN=");
    stringBuilder.append(result.getToken());

    System.out.println(stringBuilder.toString());
  }
}
