
package com.transbank.webpayserver.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para wsOneClickMulticodeReverseNullificationOutput complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="wsOneClickMulticodeReverseNullificationOutput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://service.wswebpay.webpay.transbank.com/}baseBean"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reversed" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="reverseCode" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wsOneClickMulticodeReverseNullificationOutput", propOrder = {
    "reversed",
    "reverseCode"
})
public class WsOneClickMulticodeReverseNullificationOutput
    extends BaseBean
{

    protected boolean reversed;
    protected Long reverseCode;

    /**
     * Obtiene el valor de la propiedad reversed.
     * 
     */
    public boolean isReversed() {
        return reversed;
    }

    /**
     * Define el valor de la propiedad reversed.
     * 
     */
    public void setReversed(boolean value) {
        this.reversed = value;
    }

    /**
     * Obtiene el valor de la propiedad reverseCode.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReverseCode() {
        return reverseCode;
    }

    /**
     * Define el valor de la propiedad reverseCode.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReverseCode(Long value) {
        this.reverseCode = value;
    }

}
