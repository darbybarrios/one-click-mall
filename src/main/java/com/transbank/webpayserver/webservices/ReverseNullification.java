
package com.transbank.webpayserver.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para reverseNullification complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="reverseNullification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="input" type="{http://service.wswebpay.webpay.transbank.com/}wsOneClickMulticodeReverseNullificationInput" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reverseNullification", propOrder = {
    "input"
})
public class ReverseNullification {

    protected WsOneClickMulticodeReverseNullificationInput input;

    /**
     * Obtiene el valor de la propiedad input.
     * 
     * @return
     *     possible object is
     *     {@link WsOneClickMulticodeReverseNullificationInput }
     *     
     */
    public WsOneClickMulticodeReverseNullificationInput getInput() {
        return input;
    }

    /**
     * Define el valor de la propiedad input.
     * 
     * @param value
     *     allowed object is
     *     {@link WsOneClickMulticodeReverseNullificationInput }
     *     
     */
    public void setInput(WsOneClickMulticodeReverseNullificationInput value) {
        this.input = value;
    }

}
