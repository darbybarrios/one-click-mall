
package com.transbank.webpayserver.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para wsOneClickMulticodeFinishInscriptionOutput complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="wsOneClickMulticodeFinishInscriptionOutput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://service.wswebpay.webpay.transbank.com/}baseBean"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cardExpirationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cardOrigin" type="{http://service.wswebpay.webpay.transbank.com/}cardOrigin" minOccurs="0"/&gt;
 *         &lt;element name="cardType" type="{http://service.wswebpay.webpay.transbank.com/}creditCardType" minOccurs="0"/&gt;
 *         &lt;element name="responseCode" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="tbkUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wsOneClickMulticodeFinishInscriptionOutput", propOrder = {
    "authorizationCode",
    "cardExpirationDate",
    "cardNumber",
    "cardOrigin",
    "cardType",
    "responseCode",
    "tbkUser"
})
public class WsOneClickMulticodeFinishInscriptionOutput
    extends BaseBean
{

    protected String authorizationCode;
    protected String cardExpirationDate;
    protected String cardNumber;
    @XmlSchemaType(name = "string")
    protected CardOrigin cardOrigin;
    @XmlSchemaType(name = "string")
    protected CreditCardType cardType;
    protected int responseCode;
    protected String tbkUser;

    /**
     * Obtiene el valor de la propiedad authorizationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * Define el valor de la propiedad authorizationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationCode(String value) {
        this.authorizationCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cardExpirationDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardExpirationDate() {
        return cardExpirationDate;
    }

    /**
     * Define el valor de la propiedad cardExpirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardExpirationDate(String value) {
        this.cardExpirationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad cardNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Define el valor de la propiedad cardNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad cardOrigin.
     * 
     * @return
     *     possible object is
     *     {@link CardOrigin }
     *     
     */
    public CardOrigin getCardOrigin() {
        return cardOrigin;
    }

    /**
     * Define el valor de la propiedad cardOrigin.
     * 
     * @param value
     *     allowed object is
     *     {@link CardOrigin }
     *     
     */
    public void setCardOrigin(CardOrigin value) {
        this.cardOrigin = value;
    }

    /**
     * Obtiene el valor de la propiedad cardType.
     * 
     * @return
     *     possible object is
     *     {@link CreditCardType }
     *     
     */
    public CreditCardType getCardType() {
        return cardType;
    }

    /**
     * Define el valor de la propiedad cardType.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCardType }
     *     
     */
    public void setCardType(CreditCardType value) {
        this.cardType = value;
    }

    /**
     * Obtiene el valor de la propiedad responseCode.
     * 
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * Define el valor de la propiedad responseCode.
     * 
     */
    public void setResponseCode(int value) {
        this.responseCode = value;
    }

    /**
     * Obtiene el valor de la propiedad tbkUser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTbkUser() {
        return tbkUser;
    }

    /**
     * Define el valor de la propiedad tbkUser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTbkUser(String value) {
        this.tbkUser = value;
    }

}
