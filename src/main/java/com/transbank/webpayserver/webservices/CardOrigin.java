
package com.transbank.webpayserver.webservices;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para cardOrigin.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="cardOrigin"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NATIONAL_CARD"/&gt;
 *     &lt;enumeration value="FOREIGN_CARD"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "cardOrigin")
@XmlEnum
public enum CardOrigin {

    NATIONAL_CARD,
    FOREIGN_CARD;

    public String value() {
        return name();
    }

    public static CardOrigin fromValue(String v) {
        return valueOf(v);
    }

}
