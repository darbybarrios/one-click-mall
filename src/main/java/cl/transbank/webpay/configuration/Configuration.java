package cl.transbank.webpay.configuration;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import cl.transbank.webpay.Webpay;

import java.util.ArrayList;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Configuration {

  private String privateKey;
  private String publicCert;
  private String webpayCert;
  private String commerceCode;
  private String commerceMail;
  private Webpay.Environment environment = Webpay.Environment.INTEGRACION;

  public static Configuration forTestingOneClickMall() {
    Configuration configuration = new Configuration();
    configuration.setCommerceCode("597044444429");
    configuration.setPrivateKey(
        "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEAk/sk6ps28p4xdS/LD8RBH3mZ44M998AWuKKFf8EURXbkNyO3\n" +
            "ROIk+pH09ilqjymSZMAG/acbjO+un7w/YrbDR6r6rS9ugXEFtuf/qnuGKsgArLjr\n" +
            "J0RpaFyUUaWx8UaVNpcI08j+UaQ7Zi6l65sT09RFrRpSN0SnWlgAofGCVhVUBFEf\n" +
            "zTYrTPKH5q9tDb9RFs20jH0ln5jkd4bqMz1MSsTIuDh7glV2iU0vzLti6feGUCRO\n" +
            "95z593larAVRYaiC+ay7g+PavcSKSmfV76pNnQz9Df/mLsZvSWyOQSNqVI1KoCU4\n" +
            "s+CXqSO9kyYBnJNnOI3Za3DQGZ8n9J6HK6vaVwIDAQABAoIBAESaEx0n46tKJwd6\n" +
            "ppqiksYyq2LLvviR9naV8WmuauVhHp9pUVb/t8Dp1TQhMyM3VISceSEyTRVjMAMh\n" +
            "VAlBTPp2i0uqR4J+kPWgS+gO574Bhj6MY4eKsYsOEDxhokKGRDpQfmkgLrZOerL+\n" +
            "QTEebWywZbTAFQlGLIc3cOOigJfDCv12QVqU78pVhVIKPshEqM5knrIkAqR4U5HZ\n" +
            "e/nJpT/eORGsgm6Fxg85sGr67wJRRERF0bhXvLDta1Wjt1hqaj0KQgtz6lgIt9ca\n" +
            "oWFbbUxPi1LdK8+3itiVD3XBmzyx+TywgccYjx8xt6sjff12EXkc4C7p+nk666ZM\n" +
            "p/kpTMECgYEAxGwb48gPBbis1ruhObB81nlKtd8xtfP6w0ud/BBqlKQvvK9tdPeI\n" +
            "JDnm7Kz+S+uuEv2EmH5mVF1k3EVSzhtnLQrFZwBF2IZBr+N71N7Cai6EVx3yvKE9\n" +
            "Ctt31xn8TIapCz7OuANUrGHfgwgj3yqELD9Wnn9seC7Kw7HhxQAPFkcCgYEAwN2l\n" +
            "F3pOYBQzw6zr2WL+fRVrSqgU2IDOJA0/P4EHiji6WYEWN/o+Pg/4aA7gCodxllQS\n" +
            "iv5mafCdVJlc2Gj496IwUv/4FbLCIj50wTaubyND6WBVKmNT4dd3tNIKcVIWAymC\n" +
            "VL4ksr3hml+jXaONttfczsz9rNd0cgBwTFvmU3ECgYBHmq2UHPM0/7yqTx4YHLl1\n" +
            "qiPNJz9E+Fs65I8EO6vO4gew/jalxDLyGdWQBAR0Asejjp3OxY1iReWl0vO7xQuY\n" +
            "xOtvgA72PwMlLoWO9WHkOW7vHzDFCPZqxdTrKkLqVtC7E7tiI3yzlr5XUxlYmnQy\n" +
            "eSiHgSY2rcAnFOqb2UimswKBgQCitdtkFUEt+Z06RharzcKZ0Io179u2U+SVsb9c\n" +
            "WgT8baN5g1g1XZ9JSLvvPi1xdJc1ljQWo03Prrls9/3GbbktYd2XCHBKDgeOUFa7\n" +
            "8t/yzzOqHiV7n1TTQa6yFEnPFMuRW2GjEM9xJJTCYlE23JbgXPjiVzrkqNiKnSgg\n" +
            "o5+LYQKBgCOMGq6HcWz9GtfNuATtijhJcUTkva7QcSNBNtQ9uSsN65q895GnM6yq\n" +
            "NaGPc0tptF1kKEh4L7Xp0JxNzusRBx8wMD6RqtWdZyKmGqAjbFBOIqCcg48fwYI+\n" +
            "f28BH7PHWLjem2z22L+iyK8+nS85ObyXEqANFPH+Q6yVyhbT2qV5\n" +
            "-----END RSA PRIVATE KEY-----"
    );
    configuration.setPublicCert(
        "-----BEGIN CERTIFICATE-----\n" +
            "MIIC/jCCAeYCCQDIzo8nr6Zf3zANBgkqhkiG9w0BAQsFADBBMQswCQYDVQQGEwJD\n" +
            "TDENMAsGA1UECAwEU1RHTzEMMAoGA1UECgwDVEJLMRUwEwYDVQQDDAw1OTcwNDQ0\n" +
            "NDQ0MjkwHhcNMTgxMjE5MjA1MDM1WhcNMjIwNzExMjA1MDM1WjBBMQswCQYDVQQG\n" +
            "EwJDTDENMAsGA1UECAwEU1RHTzEMMAoGA1UECgwDVEJLMRUwEwYDVQQDDAw1OTcw\n" +
            "NDQ0NDQ0MjkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCT+yTqmzby\n" +
            "njF1L8sPxEEfeZnjgz33wBa4ooV/wRRFduQ3I7dE4iT6kfT2KWqPKZJkwAb9pxuM\n" +
            "766fvD9itsNHqvqtL26BcQW25/+qe4YqyACsuOsnRGloXJRRpbHxRpU2lwjTyP5R\n" +
            "pDtmLqXrmxPT1EWtGlI3RKdaWACh8YJWFVQEUR/NNitM8ofmr20Nv1EWzbSMfSWf\n" +
            "mOR3huozPUxKxMi4OHuCVXaJTS/Mu2Lp94ZQJE73nPn3eVqsBVFhqIL5rLuD49q9\n" +
            "xIpKZ9Xvqk2dDP0N/+Yuxm9JbI5BI2pUjUqgJTiz4JepI72TJgGck2c4jdlrcNAZ\n" +
            "nyf0nocrq9pXAgMBAAEwDQYJKoZIhvcNAQELBQADggEBADQ90CTuyx6GcYWbnWC6\n" +
            "HMq/1XbS5sGrnDE99KbG10+r2vip+sfB+EXPv1AKwatcrVtvFQzQJfb+3xSqy//s\n" +
            "H0EKaXlwyoxe+KBmwogHXitqDory9BcESPp0X/fRpeJ7fVgPexL5WPu3cmfjzskc\n" +
            "ckDGNRP+rbnMQJ0DiagTFTvQu3Ab63b8DqqwYPAU+zLehfTZIlbPHHneCsnL+hiw\n" +
            "siJe7tkgHUiW5qLf5HVNl6oMPqZfwF8OF3ei3i5GBYnZ14471ddnTfQ4Vl9mLvXy\n" +
            "jXlUwEcYUR/bblp++tDgDhHkf3QXwrNuqa9NcVIYgD6kzOZpCCatDqXxLNPI/Xp7\n" +
            "RSY=\n" +
            "-----END CERTIFICATE-----\n"
    );
    return configuration;
  }

}

