
package com.transbank.webpayserver.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para initInscription complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="initInscription"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="input" type="{http://service.wswebpay.webpay.transbank.com/}wsOneClickMulticodeInitInscriptionInput" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "initInscription", propOrder = {
    "input"
})
public class InitInscription {

    protected WsOneClickMulticodeInitInscriptionInput input;

    /**
     * Obtiene el valor de la propiedad input.
     * 
     * @return
     *     possible object is
     *     {@link WsOneClickMulticodeInitInscriptionInput }
     *     
     */
    public WsOneClickMulticodeInitInscriptionInput getInput() {
        return input;
    }

    /**
     * Define el valor de la propiedad input.
     * 
     * @param value
     *     allowed object is
     *     {@link WsOneClickMulticodeInitInscriptionInput }
     *     
     */
    public void setInput(WsOneClickMulticodeInitInscriptionInput value) {
        this.input = value;
    }

}
